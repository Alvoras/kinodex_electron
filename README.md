# Desktop Kinodex 1.0.1

   ## <b>Description</b>  
   Manage a list of film that you want to watch and mark them as viewed as you catch up !

   ## <b>Please note</b> :
   - This is the desktop version.

   - The default database configuration is : root / toor. Change it in config/<b>config.json</b>

## <b>- Node.js </b>
   Please use
    ```npm install``` inside the root directory to install all the dependencies needed for the app to work.

 ## <b>- Database </b>
   I'm using MySql for this project. You can find an export of the (really simple) database in the root directory (kinodex.sql). You can either use phpmyadmin to import it easily or the mysql client of your liking.
