const express = require('express');
const router = express.Router();
const mysql = require('mysql');
const fs = require('fs');

const config = JSON.parse(fs.readFileSync('config.json'));

// SETUP MYSQL CONNECTION
var connection = mysql.createConnection({
   host: config.server.host,
   user: config.mysql.user,
   password: config.mysql.password,
   database: config.mysql.database
})

connection.connect();



module.exports = router;
