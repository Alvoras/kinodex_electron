const mysql = require('mysql');
const fs = require('fs');

const config = JSON.parse(fs.readFileSync(__dirname+'/config.json'));

function db(req, res, next) {
  const dbParams = {
    host: config.server.host,
    user: config.mysql.user,
    password: config.mysql.password,
    database: config.mysql.database
  };

    var connection = mysql.createConnection( dbParams );

    connection.connect();
    return connection;
}

module.exports = db;
