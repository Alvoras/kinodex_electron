module.exports = () => {
    const express = require('express');
    const router = express.Router();
    const path = require('path');
    const favicon = require('serve-favicon');
    const logger = require('morgan');
    const cookieParser = require('cookie-parser');
    const bodyParser = require('body-parser');
    const imdb = require('imdb-api');
    const fs = require('fs');

    const db = require('./config/db');
    const connection = db();

    const config = JSON.parse(fs.readFileSync('config.json'));

    const app = express();

    // SETUP SOCKET SERVER
    const server = require('http').Server(app);
    const io = require('socket.io')(server);

    // view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs');

    // uncomment after placing your favicon in /public
    //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, 'public')));

    /* GET home page. */
    app.get('/', function(req, res, next) {
      connection.query("SELECT * FROM films", (error, results, fields) => {
        if (error) throw error;

        let emptyState = 0;
        if (results.length === 0) emptyState = 1;

        console.log(results);
        res.render('../views/index', { isEmpty: emptyState, data: results });
      });
    });


    // --- CREATE SOCKET ---
    io.on('connection', (socket) => {
      socket.on('search', (film) => {
        console.log(film);
        // IMDB API
        imdb.search({title: film}, {apiKey: 'b1d3536d', timeout: 10000}).then( (data) => {
          console.log(data);
          socket.emit("result", data);
        } ).catch((err) => {
          socket.emit("notFound", err);
        }); // ERROR HANDLER END
      }); // SOCKET ON SEARCH END

      socket.on("probeDB", (data) => {
        console.log(data);

        // MYSQL QUERY
        connection.query("SELECT "+data.field+" FROM films WHERE title='"+data.title+"'", (error, results, fields) => {
          if (error) throw error;

          // EMIT PROBE RESULT
          if (data.field === "is_deleted") {
            (results.length === 0 && data.field === "is_deleted")? socket.emit("probeDBResult", {is_deleted: -1}):socket.emit("probeDBResult", {is_deleted: results[0].is_deleted});
          }else if (data.field === "is_viewed") {
            socket.emit("probeDBResult", {is_viewed: results[0].is_viewed});
          }
        });
      });

      // MANAGEDB HANDLING
      socket.on("add", (film) => {
        console.log("FILM :",film.title);

        let addQuery = "INSERT INTO films(title, year, poster) VALUES('"+film.title+"', "+film.year+", '"+film.poster+"')";
        console.log(addQuery);
        connection.query(addQuery, (error, results, fields) => {
          let success = 1;
          if (error) throw error, success = 0;
          socket.emit("manageOK", {success: success});
          console.log(results);
        });
      }); // END OF SOCKET ON ADD

      socket.on("restore", (title) => {
        manageFilmState(0, 0, title); // MARK AS NOT DELETED
      });

      socket.on("delete", (title) => {
        manageFilmState(1, 0, title); // MARK AS DELETED
        manageFilmState(0, 1, title); // MARK AS NOT VIEWED
      });

      socket.on("not_viewed", (title) => {
        manageFilmState(0, 1, title); // MARK AS NOT VIEWED
      });

      socket.on("viewed", (title) => {
        manageFilmState(1, 1, title); // MARK AS VIEWED
      });


      function manageFilmState(value, mode, title) {
        let queryField = "is_viewed";
        if ( !(mode) ) {
          queryField = "is_deleted";
        }

        let dbQuery = "UPDATE films SET "+queryField+"="+value+" WHERE title='"+title+"'";
        console.log(dbQuery);

        connection.query(dbQuery, (error, results, fields) => {
          let success = 1;
          if (error) throw error, success = 0;
          socket.emit("manageOK", {success: success});
          console.log(results);
        });
      } // END OF FUNCTION manageFilmState

    }); // END OF SOCKET ON CONNECTION

    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
      var err = new Error('Not Found');
      err.status = 404;
      next(err);
    });

    // error handler
    app.use(function(err, req, res, next) {
      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};

      // render the error page
      res.status(err.status || 500);
      res.render('error');
    });

    server.listen(config.server.port) // 8080

}
