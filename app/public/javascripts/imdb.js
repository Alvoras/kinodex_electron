// --- NODEJS STYLE JS ---

function fetchData(socket) {
   var socket = io.connect("http://localhost:8080");

   displayLoader();

   let film = $("#search-field").val();

   socket.emit("search", film); // GET IDs OF RESULTS FILMS

   socket.on("notFound", (errorLog) => {
      console.log(errorLog);
      hideLoader();
      let text = "No films found :(";
      let type = "no-results";

      newNotification(text, type);
   })

   socket.on("result", (data) => { // ON IDs RECEIVE
      let maxSize = data.results.length;

      if (maxSize > 10) maxSize = 10;

      for (var i = 0; i < maxSize; i++) {
         console.log(data.results[i]);
         let film = [];
         let movieInfo = data.results[i];
         // SKIP NON-MOVIE FIND
         if (movieInfo.type !== "movie" && movieInfo.type !== "series" && movieInfo.type !== "documentary") {
            continue;
         }
         // GET FILM DETAILS
         film["title"]= movieInfo.title.replace("'", "\\'");
         console.log("HERE", film["title"]);
         film["year"] = movieInfo.year;
         film["id"] = movieInfo.imdbid;
         if (movieInfo.poster !== "N/A") {
            film["poster"] = movieInfo.poster;
         }else{
            film["poster"] = "../images/noposter.png";
            console.log("empty poster");
         }

         console.log(film);
         createTopTile(film, socket);
      }
      hideLoader();

      // ADD OPACITY CALLBACK ?
   }); // END OF SOCKET.ON result
}
function searchPurge() {
   let target = document.getElementById('result-row');
   target.innerHTML='';
}
function toggleFilm(el, mode) {
   var socket = io.connect("http://localhost:8080");

   readyChangeState(el, "refresh");

   let overlayContent = $(el).children().children();

   let film = [];

   film["title"] = overlayContent[0].innerHTML;
   film["year"] = overlayContent[2].firstChild.innerText;
   film["poster"] = $(overlayContent).parent().parent().css("background-image").replace("url(\"", "").replace("\")", "")

   let params = {field: mode, title: film["title"]};

   socket.emit("probeDB", params);

   socket.on("probeDBResult", (data) => {
      console.log("EHRE", data);
      //SOCKET EMIT PROBE DB
         // ON RECEIVE DBPROBERESULT
            // IF PROBE DB == 1 THEN RESTORE
               // IS_DELETED == TRUE
               // MANAGEDB(MODE === RESTORE)

            // IF PROBE DB == 0 THEN DELETE
               // IS_DELETED == FALSE
               // MANAGEDB(MODE === DELETE)

            // IF PROBE DB == -1 THEN ADD
               // FILM NOT IN DB
               // MANAGEDB(MODE === ADD)

      manageDb(data, mode, film, el);
   })
}
function manageDb(data, mode, film, el) {
   console.log(el);
   console.log(data);
   var socket = io.connect("http://localhost:8080");
   if (mode === "is_deleted") {
      switch (data.is_deleted) {
         case -1:
            socket.emit("add", {title: film["title"], year: film["year"], poster: film["poster"]});
            socket.on("manageOK", (data) => {
               console.log(data);
               console.log(el);
               if ( (data.success) ) {
                  let text = "Film added successfully";
                  let type = "success";

                  newNotification(text, type);
                  readyChangeState(el, "check");
                  // LIVE ADD CARD
                  createMainCard(film);
               }else {
                  let text = "Failed to add the film";
                  let type = "fail";

                  newNotification(text, type);

                  readyChangeState(el, "times");
               }
            });
            break;
         case 0:
            if ($(el).hasClass("top-film-tile")) {
               let text = "Film already in your list !";
               let type = "fail";

               newNotification(text, type);
               readyChangeState(el, "times");
            }else {
               socket.emit("delete", film["title"]);
               let text = "Film successfully removed";
               let type = "success";

               newNotification(text, type);
            }
            break;
         case 1:
            socket.emit("restore", film["title"]);
            socket.on("manageOK", (data) => {
               console.log(data);
               console.log(el);
               if ( (data.success) ) {
                  let text = "Film restored successfully !";
                  let type = "success";

                  newNotification(text, type);
                  readyChangeState(el, "check");
                  // LIVE ADD CARD
                  createMainCard(film);
               }else {
                  let text = "Failed to restore the film"
                  let type = "fail";

                  newNotification(text, type);

                  readyChangeState(el, "times");
               }
            });
            break;
         default:
         let text = "Something went wrong";
         let type = "fail";

         newNotification(text, type);
         console.log("ERROR (manageDb) -- ENTERED DEFAULT SWITCH");
         break;
      }
   }else if (mode === "is_viewed") {
      switch (data.is_viewed) {
         case 0:
            // MARK AS VIEWED
            socket.emit("viewed", film["title"])
            break;
         case 1:
            // MARK AS NOT VIEWED
            socket.emit("not_viewed", film["title"])
            break;
         default:

      }
   }
}
